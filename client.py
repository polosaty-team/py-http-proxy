# -*- coding: utf-8 -*-
import tornado
from tornado import gen, web, httpclient
from cPickle import loads, dumps
import urllib

REQUESTS = {}
class Monkey(object):
    def __init__(self, server=None, ioloop=None):
        self.ioloop = ioloop or tornado.ioloop.IOLoop.current()
        self.run = False
        self.server = server

    # @web.asynchronous
    @gen.coroutine
    def get_cmds(self):
        print 'get_cmds'
        client = httpclient.AsyncHTTPClient()
        
        def handle_cmd(cmd):
            cmd = loads(cmd)
            print 'cmd:', cmd 
            if isinstance(cmd, dict):
                REQUESTS[cmd['id']] = cmd
                self.handle(cmd['id'])

        req = httpclient.HTTPRequest(
            url=self.server + '/cmd',
            streaming_callback=handle_cmd
        )
        try:
            yield client.fetch(req)
        except Exception as e:
            print 'Exception', repr(e)

    def stop(self):
        print 'stoping monkey'
        self.run = False

    @gen.coroutine
    def start(self):
        print 'monkey started'
        self.run = True
        while self.run:
            yield self.get_cmds()
            yield gen.sleep(0.5)
    
    # @web.asynchronous
    @gen.coroutine
    def handle(self, req_id):   
        print 'handle', req_id
        client = httpclient.AsyncHTTPClient()
        # response = yield httpclient.AsyncHTTPClient().fetch(url)
        request = REQUESTS[req_id]
        
        # import pudb; pu.db
        def streaming_callback(resp_data):
            self.response(req_id, resp_data)

        if 'GET' in request:
            req = httpclient.HTTPRequest(
                *request['GET'].get('args', []),
                **dict(request['GET'].get('kwargs', {}), streaming_callback=streaming_callback)
                
            )
        else:
            REQUESTS.pop(req_id)
            raise gen.Return()

        try:
            resp_data = yield client.fetch(req)
            # yield self.response(req_id, resp_data.buffer.read())
            # `map()` doesn't return a list in Python 3
            # yield list(map(client.fetch, requests))
        except Exception as e:
            print('Exception: %s %s' % (e, request))
            yield self.response(req_id, resp_data.buffer.read(), 502)
            # raise gen.Return(None)

    @gen.coroutine
    def response(self, req_id, resp_data, code=200):
        print 'response', req_id, resp_data, code
        # import pudb; pu.db
        post_data = urllib.urlencode({'r': dumps(resp_data), 'id': req_id})
        req = httpclient.HTTPRequest(
            url=self.server + '/resp',
            method='POST', 
            body=post_data
        )
        client = httpclient.AsyncHTTPClient()
        yield client.fetch(req)
        # print re


def main():
    ioloop = tornado.ioloop.IOLoop.current()
    monkey = Monkey(server='http://127.0.0.1:8888', ioloop=ioloop)
    monkey.start()
    try:
        print "Started"
        ioloop.start()
    except KeyboardInterrupt:
        print "Stopings all"
        monkey.stop()
        ioloop.stop()
    

if __name__ == "__main__":
    main()
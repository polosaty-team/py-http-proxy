# -*- coding: utf-8 -*-

from tornado import gen, web, httpclient
import tornado
from cPickle import dumps, loads
from tornado.concurrent import Future

'''
server: send_request >
client: get_request < 
clinet: get_chunk 1 <
client: send_chunk 1 >
server: get_chunk 1<
clinet: get_chunk 2 <
client: send_chunk 2 >
server: get_chunk 2<

'''
class ProxyHandler(web.RequestHandler):
    @web.asynchronous
    @gen.coroutine
    def get(self, *args, **kwargs):
        # data = self.get_argument('body', 'No data received')
        
        print 'proxy', args, kwargs

        yield ADD_REQUEST({'GET': {'kwargs': kwargs}}, self)
        # import pudb; pu.db
        self.write('OK')
        print REQUESTS_IN



class ResponceHandler(web.RequestHandler):
    @web.asynchronous
    @gen.coroutine
    def post(self, *args, **kwargs):
        # data = self.get_argument('body', 'No data received')
        req_id = self.get_body_argument('id')
        if not req_id:
            return

        try:
            r = loads(self.get_body_argument('r').encode('utf-8'))
        except Exception as ex:
            import pudb; pu.db

        try:
            req_id = int(req_id)
            if req_id in REQUESTS_WORK and REQUESTS_WORK[req_id]['handler']:
                import pudb; pu.db
                REQUESTS_WORK[req_id]['handler'].write(r)
                REQUESTS_WORK[req_id]['handler'].flush()
        except Exception as ex:
            import pudb; pu.db

        # data = self.request.body or 'No data received'
        # # self.write(data)
        # print "post", data
        # import pudb; pu.db
        # pass
        print req_id, r


DEFAULT_CMD = 'noop'

# REQUESTS_IN = {1: {'GET': {'kwargs': {'url': 'http://ya.ru'}}, 'id': 1}}

REQUESTS_IN = {} 
REQUESTS_WORK = {}
ID = 1
def ADD_REQUEST(req, handler=None):
    global ID
    req['id'] = ID
    REQUESTS_IN[ID] = req
    f = Future()
    REQUESTS_WORK[ID] = {'future':f, 'handler': handler} 
    ID += 1
    print REQUESTS_IN
    print REQUESTS_WORK
    return f

ADD_REQUEST({'GET': {'kwargs': {'url': 'http://127.0.0.1:8080/test'}}})


class CmdHandler(web.RequestHandler):
    @web.asynchronous
    @gen.coroutine
    def get(self):
        client = httpclient.AsyncHTTPClient()

        # self.write('\n' + '-' * 60 + '\n')
        # self.flush()

        # requests = [
        #     httpclient.HTTPRequest(
        #         url='http://httpbin.org/delay/' + str(delay),
        #         streaming_callback=self.on_chunk
        #     ) for delay in [5, 4, 3, 2, 1]
        # ]

        # # `map()` doesn't return a list in Python 3
        # yield list(map(client.fetch, requests))
        # yield gen.sleep(1)
        k = REQUESTS_IN.keys()
        if k:
            req = REQUESTS_IN.pop(k[0])
            # REQUESTS_WORK[k[0]] = req
            self.write(dumps(req))
        else:
            self.write(dumps(DEFAULT_CMD))

        self.finish()

    def on_chunk(self, chunk):
        self.write('some chunk<br>')
        self.flush()


def main():
    application = tornado.web.Application([
        (r"/cmd", CmdHandler),
        (r"/resp", ResponceHandler),
        (r"(?P<url>http://.*)$", ProxyHandler),

    ])
    application.listen(8888)
    try:
        print "Started"
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        print "Stopings all"
        tornado.ioloop.IOLoop.current().stop()
    




if __name__ == "__main__":
    main()
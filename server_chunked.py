# -*- coding: utf-8 -*-

from tornado import gen, web, httpclient
import tornado


class StreamingHandler(web.RequestHandler):
    @web.asynchronous
    @gen.coroutine
    def get(self):
        # client = httpclient.AsyncHTTPClient()

        self.write('\n' + '-' * 60 + '\n')
        self.flush()

        # requests = [
        #     httpclient.HTTPRequest(
        #         url='http://httpbin.org/delay/' + str(delay),
        #         streaming_callback=self.on_chunk
        #     ) for delay in [5, 4, 3, 2, 1]
        # ]

        # `map()` doesn't return a list in Python 3
        # yield list(map(client.fetch, requests))
        for i in xrange(3):
            yield gen.sleep(1)
            self.write('some_chunk_%s<br>\n' % i)
            self.flush()

        self.finish()

    def on_chunk(self, chunk):
        self.write('some chunk<br>')
        self.flush()


def main():
    application = tornado.web.Application([
        (r"/test", StreamingHandler),
    ])
    application.listen(8080)
    try:
        print "Started"
        tornado.ioloop.IOLoop.current().start()
    except KeyboardInterrupt:
        print "Stopings all"
        tornado.ioloop.IOLoop.current().stop()
    


if __name__ == "__main__":
    main()